import React from 'react';
import './App.css';
import Dashboard from './Containers/Dashboard/Dashboard';

function App() {
  return (
     <Dashboard />
  );
}

export default App;
