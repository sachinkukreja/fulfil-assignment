import React from 'react'
import styles from './Card.module.css'

class Card extends React.Component {



    render(){
    return (<div className={styles.container} >
        <p className={styles.title}>{this.props.title?this.props.title:''}</p>
        <div className={styles.card}>
            {this.props.children}
        </div>
    </div>)
    }
}
export default Card
