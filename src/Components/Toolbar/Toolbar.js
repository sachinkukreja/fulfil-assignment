import React from 'react'
import styles from './Toolbar.module.css'

export const Toolbar = () =>{
return ( <header className={styles.toolbar}>
    <p>Fulfil.io List Assignment</p>
</header>)
}