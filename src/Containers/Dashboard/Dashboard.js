import React from 'react'
import styles from './Dashboard.module.css'
import { Toolbar } from '../../Components/Toolbar/Toolbar';
import Card from '../../Components/Card/Card';
import DataTable from '../DataTable/DataTable';
import DataList from '../DataTable/DataList';


class Dashboard extends React.Component {

    constructor(props){
        super(props);

        this.state={
            apiResponseData : [],
            photos:[],
            startIndex:0,
            numberOfItemsToAdd:10
        }
    }

    componentDidMount(){
        fetch("https://jsonplaceholder.typicode.com/photos").then(res=>res.json()).then(result=>{
          this.setState({
              apiResponseData:result,
              photos:result.slice(this.state.startIndex,this.state.numberOfItemsToAdd),
              startIndex:this.state.startIndex + this.state.numberOfItemsToAdd ,
            });
        })
    }


    loadMore = () => {
        const rowsToLoad = this.state.apiResponseData.slice(this.state.startIndex,this.state.startIndex + this.state.numberOfItemsToAdd)
        this.setState({
            photos: [...this.state.photos,...rowsToLoad],
            startIndex:this.state.startIndex + this.state.numberOfItemsToAdd ,
        })

    }

    rowClicked = (rowData,rowIndex) =>{
        console.log("Row clicked Data:::", rowData )
    }
    render() {
        return (
            <div className={styles.container}>
                <Toolbar />
                <Card title="DataTable (renders <table>)">
                    <DataTable columns={[{
                        'id': 'albumId', // Unique ID to identify column
                        'label': 'Album Id',
                        'numeric': true,
                        'width': '20px'
                    }, {
                        'id': 'title',
                        'label': 'Title',
                        'numeric': false, // Right Align
                    },{
                        'id': 'url', 
                        'label': 'Album Url',
                        'numeric': false,
                    },{
                        'id': 'thumbnailUrl', 
                        'label': 'Thumbnail Url',
                        'numeric': false,
                    }]}
                    onLastElementVisible = {this.loadMore}
                    rows={this.state.photos}
                    onRowClick={(rowData,rowIndex) => this.rowClicked(rowData,rowIndex)}
                        />
                </Card>
                    
                    <Card title="Virtualised DataList (Highly scalable and performant)">
                        <DataList 
                        columns={[{
                            'id': 'albumId', // Unique ID to identify column
                            'label': 'Album Id',
                            'numeric': true,
                            'width': '20px'
                        }, {
                            'id': 'title',
                            'label': 'Title',
                            'numeric': false, // Right Align
                        },{
                            'id': 'url', 
                            'label': 'Album Url',
                            'numeric': false,
                        },{
                            'id': 'thumbnailUrl', 
                            'label': 'Thumbnail Url',
                            'numeric': false,
                        }]}
                         rows={this.state.apiResponseData}
                         onRowClick={(rowData,rowIndex) => this.rowClicked(rowData,rowIndex)}/>
                    </Card>
            </div>
        )


    }
}

export default Dashboard