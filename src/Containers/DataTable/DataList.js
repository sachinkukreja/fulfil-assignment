import React from 'react'
import DataListItem from './DataListItem'
import { FixedSizeList as List } from 'react-window';


class DataList extends React.Component {

    onItemsRendered = ({ visibleStopIndex }) => {

        //Check when the last Visible Item in the Virtual List is equal to count, 
        //This should serve as a trigger point to get the next set of data from API
        if (visibleStopIndex === this.props.rows.length - 1) {
            console.log("load more")
        }
    }

    render() {
        return (
            <List
                onItemsRendered={this.onItemsRendered}
                height={200}
                itemCount={this.props.rows.length}
                itemSize={35}
                itemData={this.props.rows}
                >
                {DataListItem}
            </List>
        )
    }

}
export default DataList