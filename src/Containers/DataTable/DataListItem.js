import React from 'react'

class DataListItem extends React.PureComponent {
    render() {
      // Access the items array using the "data" prop:
      const item = this.props.data[this.props.index];
   
      return (
        <div style={this.props.style} className="item">
         <p className="item-p">{item.albumId}</p>
         <img className="item-img" src={item.thumbnailUrl}/>
         <p className="item-p">{item.title}</p>
          <p className="item-p">{item.url}</p>
        </div>
      );
    }
  }

  export default DataListItem