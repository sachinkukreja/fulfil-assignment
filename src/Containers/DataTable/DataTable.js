import React from 'react'
import TableRow from './TableRow';


class DataTable extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props)
        this.state = {
        numericRows:{},
        columns:[]
        }
      }

      componentDidMount(){
        if(this.props.onLastElementVisible)
        this.refs.container.addEventListener('scroll',()=>{
            if (
                this.refs.container.scrollTop + this.refs.container.clientHeight >=
                this.refs.container.scrollHeight
              ) {
                this.props.onLastElementVisible();
              }
        })

        const numericRows = {}
        this.props.columns.map(column => {
            numericRows[column.id] = column.numeric
       })

       this.setState ({numericRows,columns:this.props.columns})

      }

    // handleSelect = (e) => {
    //     const selected = this.state.selected;
    //     selected[e.target.name] = e.target.checked;
    //     this.setState({ selected });
    //   }    


    //   SelectAll = ()=>{

    //     const selectAll = !this.state.selectAll;
    //     this.setState({selectAll:selectAll})
    //     var selected = {}
    //     this.state.rows.forEach((row)=>{
      
    //         selected[row.index] = selectAll
    //     })
    //     this.setState({
    //         selected:selected
    //     })
    //   }


    renderColumnHeader(){
          return this.state.columns.map(column => {
            return <th key={column.id} width={column.width} className={column.numeric?"right":""}>{column.label}</th>
        })
    }

    renderRows(){
       return this.props.rows.map((row,index)=>{
            return(
               <TableRow 
               key={row.id} 
               data = {row}
               numericRows = {this.state.numericRows}
               rowClicked={e=>this.props.onRowClick(row,index)} 
              />
            )
        })
    }


    render(){
        return( 
               <div ref="container" style={{height:'200px',overflow:'auto'}}>
           <table>
               <thead>
               <tr>{this.renderColumnHeader()}</tr>
               </thead>
               <tbody>
               {this.renderRows()}
               </tbody>
               
           </table>
           </div >
        
            )
    }
   

}

export default DataTable