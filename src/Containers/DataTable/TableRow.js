import React from 'react'

class TableRow extends React.PureComponent {

  componentDidMount(){
    this.refs.row.addEventListener('click',()=>{
      this.props.rowClicked()
    })
  }
   
  
renderRowData(){
  return Object.keys(this.props.data).map(key=>{
    if(key === 'id')
    return ;

    return <td className={this.props.numericRows[key]?"right":""}>{this.props.data[key]}</td>

  })
}

    render() {
      return (
        <tr ref="row">
       {this.renderRowData()}
        </tr>
      );
    }
  }
  export default TableRow

TableRow.defaultProps = {
        selected: false
}